package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
	json "encoding/json"
)

type Handler struct{}

type DateResponse struct{
	currentDate string
	greeting string
}

func (handler Handler) ServeHTTP(response http.ResponseWriter, request *http.Request) {
	log.Println("request received", *request)
	
	message := DateResponse{time.Now().String(), "welcome dave"}
	byteMessage, err := json.Marshal(message)
	if(err != nil){
		log.Fatal(err)
	}
	
	fmt.Fprint(response, string(byteMessage))
}

func main() {
	var handler Handler
	err := http.ListenAndServe("localhost:9000", handler)
	if err != nil {
		log.Fatal(err)
	} 
}
